"""
File consists of several functions for the calculation rules of FAIR Quality KPIs
"""

from functions.classes import *

def test_function():
    """Test function to check module functionality"""
    print("You called the test function.")

def kpi_mass(system: LegoAssembly)->float:
    """
    Calculates the total mass of the system

    Args:
        system (LegoAssembly): LegoAssembly object that represents the system

    Returns:
        total_mass (float): Sum of masses of all components in the system in g

    Raises:
        TypeError: If an argument with unsupported type is passed
            (i.e. anything other than LegoAssembly).
    """
    if not isinstance(system, LegoAssembly):
        raise TypeError(f"Unsupported type {type(system)} passed to kpi_mass()")

    total_mass = 0
    for c in system.get_component_list(-1):
        total_mass += c.properties["mass [g]"]
    return total_mass # alternative: sum(c.properties["mass [g]"] for c in system.get_component_list(-1))


# KPI 1: Gesamtpreis

def kpi_price(system: LegoAssembly)->float:
   
    if not isinstance(system, LegoAssembly):
        raise TypeError(f"Unsupported type {type(system)} passed to kpi_price()")

    total_price = 0
    for c in system.get_component_list(-1):
        total_price += c.properties["price [Euro]"]
    return total_price

# KPI 2: Einzelteil mit der längsten Lieferzeit

def kpi_delivery_time(system: LegoAssembly) -> float:
    if not isinstance(system, LegoAssembly):
        raise TypeError(f"Unsupported type {type(system)} passed to kpi_delivery_time()")

    delivery_time = 0
    for c in system.get_component_list(-1):
        part_delivery_time = c.properties["delivery time [days]"]
        if part_delivery_time > delivery_time:
            delivery_time = part_delivery_time

    if delivery_time == 0:
        raise ValueError("Keine Lieferzeit in den Eigenschaften der Einzelteile gefunden")
    
    return delivery_time


# KPI 3: Gesamtpreis geteilt durch die Anzahl der Einzelteile

## Anzahl der Einzelteile

def kpi_amount(system: LegoAssembly) -> int:
    if not isinstance(system, LegoAssembly):
        raise TypeError(f"Unsupported type {type(system)} passed to kpi_amount()")

    return len(system.get_component_list())
    

if __name__ == "__main__":
    """
    Function to inform that functions in this module is
    intended for import not usage as script
    """
    print(
        "This script contains functions for calculating the FAIR Quality KPIs."
        "It is not to be executed independently."
    )
